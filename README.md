# Tasarım Kalıpları - Design Patterns

## Giriş

​	Günlük hayatımızda insanlarla iletişim kurmak için tercih ettiğimiz konuşma ve yazma dilinin herkes tarafından kolay bir şekilde anlaşılması ve o dili ilk defa kullanacak yeni insanlar için de kolay bir şekilde öğrenilmesi için “cümle yapısı” kavramı ortaya atılmış ve yapısına göre kategorilendirilmiştir.	Tasarım kalıpları genellikle oop altyapısına sahip diller ile kullanılabilen, daha önce karşılaşılmış problemlerin optimum çözümünü sunduğunu iddia eden kişilerin ortaya atmış oldukları prensiplerdir. Tasarım kalıplarının en büyük getirisi yazılım geliştiren insanların yazmış oldukları kodun ortak bir dile sahip olması ve diğer insanlar tarafından anlaşılması. Tabii bu duruma ek olarak daha hızlı yazılım geliştirmek ve gerekli önsezi yi kazanmak için çeşitli prensipleri(solid vb.) de bilmek ve uygulamak gerekli görünüyor.

​	1994 yılında dörtlü çete olarak adlandırılan Erich Gamma, Richard Helm, Ralph Johnson ve John Vlissides, “Design Patterns” isimli kitabı çıkarmışlar. Bu kitapta tasarım kalıpları 3 farklı kategoride 23 adet olarak belirtilmiş. Yine bu kitaba göre desenler şu şekilde;

1. Behavioral Design Patterns (Davranışsal Tasarım Kalıpları): Nesneler arası ilişkilerde işlerin yerine getirilirken sınıfların nasıl davranacağı ile ilgili.

2. Creational Design Patterns (Kurucu Tasarım Kalıpları): Nesnelerin oluşturulması ve yönetilmesi ile ilgili.

3. Structural Design Patterns (Yapısal Tasarım Kalıpları): Nesneler arası ilişkiler de çalışma zamanı sırası davranışlarını belirlemek ile ilgili.

   

   Kapsamına göre grupladığımızda, sınıfları ele alan ve objeleri ele alan desenler olarak ayrılır.

   ![Screenshot_1](purpose.png)

> Her pattern in farklı karakteristik özellikleri var, her pattern ayrı bir yazının konusu. Ben bu yazıda bu patternlere kısaca değineceğim. Yazının sonunda vereceğim referans lar üzerinden detaylı inceleme yapabilir ve uml şemalarını inceleyebilirsiniz. Ek olarak eklemiş olduğum gif ler de kullanmış olduğum c# üzerinden örnek senaryoları yine yazı sonrası vereceğim adresten indirip kontrol edebilir siniz.
>

## Behavioral Design Patterns (Davranışsal Tasarım Kalıpları)

### Chain of responsibility

​	Bu pattern kısaca logic gerektiren yani bir şarta göre karar verilecek durumların tek bir fonksiyon içinde işletilmesi değilde her şartın farklı fonksiyonlar içerisinde hiyerarşik bir yapıda  işletilmesi prensibine dayanır. Bu fonksiyonlar abstract class tan çağrılır, override edilir ve belli bir sıraya göre çağrılırlar.

![ChainOfResponsibility](ChainOfResponsibility.gif)

### Command	

​	Çalıştırmak istediğimiz fonksiyonların nesneye dönüştürülerek çağırılması prensibine dayanan pattern dir.

![Command](Command.gif)



### Interpreter

​	Bu pattern belirli bir string özelinde yapılan yorumlama ve çevirmenin daha efektif bir şekilde yapılabilmesi için tasarlanmıştır.

![Interpreter](Interpreter.gif)

### Iterator

​	Koleksiyon nesnesinin elemanlarına temel yapıyı bilmeden sıralı  bir şekilde nasıl erişilir sorusunun cevabını bu pattern ile sağlayabiliriz.

![Iterator](Iterator.gif)

### Mediator

​	Bu tasarım kalıbı, birbirlerinin yapısını bilmeden birden çok nesnenin birbirleriyle nasıl iletişim kurabileceğini aralarındaki iletişimin nasıl  kurulması gerektiğini belirtir.

![Mediator](Mediator.gif)

### Memento

​	Geri al deyince akla gelecek pattern dir. Bu pattern ile nesne bellekte kopyalanır ve gerekli durumda tekrar elde edilir. Geri alma işlemlerinde sık kullanılır.

![Memento](Memento.gif)

### Observer

​	Nesne de meydana gelen değişikliklerin diğer nesnelere bildirilmesi ile ilgili pattern dir. Genellikle one to many  ilişkili nesneler ile kullanılır.

![Observer](Observer.gif)

### State

​	Nesne de meydana gelen değişiklik sonrası nesne nin farklı şekilde çalışmasını sağlar. Nesne içerisinde bir arayüzün referansı tutulur, değişiklik sonrası aynı arayüzden türeyen farklı bir nesnenin referansı atanır. 

![State](State.gif)

### Strategy

​	Runtime esnasında değişken method implementasyonlarına sahip olan bir nesne için uygun bir design pattern dir. Yani bir işlemin birden fazla şekilde gerçekleşebileceği durumları düzenler.

![Strategy](Strategy.gif)

### Template Method

​	Bu pattern de operasyonel işlemlerin bulunduğu metodlar abstract metod ta tanımlanır ve kullanılmak istenen method override edilerek kullanılır.Mantık olarak strategy pattern e benzese de uygulama da farklılıklar vardır. İki yapıda encapsulate kullanır fakat template method da inheritance söz konusudur.

![TemplateMethod](TemplateMethod.gif)

### Visitor

​	Nesne yapısını değiştirmeden bir nesne kümesi üzerinde yeni işlemler oluşturmak ve gerçekleştirmek için kullanılır.

## Creational Design Patterns (Kurucu Tasarım Kalıpları)

### Abstract Factory

​	Birbirleri ile ilişkili sınıfların tek bir interface yada abstract class ile yönetilmesini sağlamayı amaçlayan kalıptır.

![AbstractFactory](AbstractFactory.gif)

### Builder

​	Birden fazla parçadan oluşan bir nesnenin üretiminin step by step gerçekleştirilmesi ve en son istenen nesnenin geri döndürülmesi amaçlandığın da kullanılır.

![Builder](Builder.gif)

### Factory Method

​	Kalıtımsal ilişkiler taşıyan nesnelerin tek bir merkezden yönetilmesini sağlayan tasarım desenidir.

![FactoryMethod](FactoryMethod.gif)

### Prototype

​	Var olan nesnenin kopyalanması amaçlandığın da kullanılır.

![Prototype](Prototype.gif)

### Singleton

​	Uygulama yaşam süresince bir sınıfın nesnesinin yalnızca bir defa oluşmasını garanti etmek için kullanılır.

![Singleton](Singleton.gif)

## Structural Design Patterns (Yapısal Tasarım Kalıpları)

### Adapter

​	Adapter kalıbı ilgisiz sınıfların birlikte çalışmasını sağlamaya yöneliktir. Genellikle tasarlandıktan sonra sistemlere uygulanır. 

![Adapter](Adapter.gif)

### Bridge

​	Bu kalıp ile araya köprü görevi görecek interface yazılıp bu interface ile tüm implementasyonlar çağrılabilir.

![Bridge](Bridge.gif)

### Composite

​	Bu pattern ile uygun arayüzleri ve özetleri ayarlayarak, farklı nesnelere aynı şekilde davranıp ağaç yapısı kurabiliriz.

![Composite](Composite.gif)

### Decorator

​	Bu pattern ile nesnelere dinamik olarak sorumluluklar(metodlar) ekleyebiliriz.

![Decorator](Decorator.gif)

### Facade

​	Alt sistemlerin direkt olarak kullanılması yerine alt sistemdeki nesneleri kullanan başka bir nesne üzerinden kullanılmasını sağlar.

![Facade](Facade.gif)

### Flyweight

​	Flyweight modeli, oluşturulan nesne sayısını azaltmak, belleği ve kaynak kullanımını azaltmak için kullanılır. Sonuç olarak, performansı arttırır.

![Flyweight](Flyweight.gif)

### Proxy

​	Diğer nesnelere referans veren bir taşıyıcı nesne sağlamak için kullanılır.

## Diğer Bazı Kurumsal Pattern ler

### Repository Pattern

​	Veriye erişim de yani database CRUD operasyonlarında kod tekrarı yapmayı önlemek amacıyla ortaya çıkmıştır.

### CQOP(Command Query Object Pattern)

​	Repository pattern ile birlikte ortaya çıkan birçok fonksiyonun şişmesi ve single responsibility prensibinin bozulmaya başlaması sonrası doğan ihtiyaç ile bu pattern ortaya atılmış. Bu pattern e göre db erişiminde her class tek bir işi yapar.

### Unit Of Work Pattern

​	Veri tabanı üzerinde gerçekleşecek işlemlerintek bir kanal üzerinde toplu halde gerçekleşmesi için tasarlanmış pattern dir.

## Architectural Patterns Vs Design Patterns

​	Yazılım dünyasında mimari kalıplar ile tasarım kalıpları birbirine çok karıştırılır. Tasarım kalıpları kod tabanının belirli bir bölümünü etkilerken, Mimari kalıplar, büyük ölçekli bileşenleri, sistemin genel özelliklerini ve mekanizmalarını ilgilendiren üst düzey stratejiler dir. Architectural pattern e en bilinen örnek MVC pattern i verilebilir.



> Yazıda kullanılan Kaynaklar
> [https://exceptionnotfound.net](https://exceptionnotfound.net/)
> [https://www.dofactory.com](https://www.dofactory.com/)
> [http://harunozer.com](http://harunozer.com/)
> [https://www.dotnettricks.com](https://www.dotnettricks.com/)
> https://en.wikipedia.org/wiki/Design_Patterns

Yazıda kullanılan Örnekleri oluşturduğum şu projeden inceleyebilir siniz.## Buraya şu proje github link gelicek##