﻿
using System.Collections.Generic;
using System.Linq;

namespace DesignPatterns
{
    //Mock Db
    public static class MockWidgetDatabase
    {
        public static List<Widget> Widgets { get; }
        static MockWidgetDatabase()
        {
            Widgets = new List<Widget>()
            {
                new Widget {Id = 1, Name = "Widget 1", Shape = "Shape 1"},
                new Widget {Id = 2, Name = "Widget 2", Shape = "Shape 2"},
                new Widget {Id = 3, Name = "Widget 3", Shape = "Shape 3"}
            };
        }
    }

    public class Widget
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Shape { get; set; }
    }

    public interface IQuery<out TResponse> { }
    public interface IQueryHandler<in TQuery, out TResponse> where TQuery : IQuery<TResponse>
    {
        TResponse Get();
    }

    public class AllWidgetsQuery : IQuery<IEnumerable<Widget>> { }

    public class AllWidgetsQueryHandler : IQueryHandler<AllWidgetsQuery, IEnumerable<Widget>>
    {
        public IEnumerable<Widget> Get()
        {
            return MockWidgetDatabase.Widgets.OrderBy(w => w.Name);
        }
    }

    public static class WidgetQueryHandlerFactory
    {
        public static IQueryHandler<AllWidgetsQuery, IEnumerable<Widget>> Build(AllWidgetsQuery query)
        {
            return new AllWidgetsQueryHandler();
        }

        public static IQueryHandler<OneWidgetQuery, Widget> Build(OneWidgetQuery query)
        {
            return new OneWidgetQueryHandler(query);
        }
    }
    ////

    public class OneWidgetQuery : IQuery<Widget>
    {
        public int Id { get; private set; }
        public OneWidgetQuery(int id)
        {
            Id = id;
        }
    }

    public class OneWidgetQueryHandler : IQueryHandler<OneWidgetQuery, Widget>
    {
        private readonly OneWidgetQuery _query;
        public OneWidgetQueryHandler(OneWidgetQuery query)
        {
            _query = query;
        }
        public Widget Get()
        {
            return MockWidgetDatabase.Widgets.FirstOrDefault(w => w.Id == _query.Id);
        }
    }

}
