﻿using System;

namespace DesignPatterns.Proxy
{
    class Program_CommanQuery
    {
        static void Main_CommanQuery(string[] args)
        {
            var queryGetAll = new AllWidgetsQuery();
            var handler1 = WidgetQueryHandlerFactory.Build(queryGetAll);

            var widgets = handler1.Get();
            foreach (var widgetItem in widgets)
            {
                Console.WriteLine($"Id: {widgetItem.Id} - Name:{widgetItem.Name}");
            }

            var query = new OneWidgetQuery(3);
            var handler = WidgetQueryHandlerFactory.Build(query);
            var widget = handler.Get();
            Console.WriteLine(widget.Shape);
            Console.ReadKey();
        }
    }
}