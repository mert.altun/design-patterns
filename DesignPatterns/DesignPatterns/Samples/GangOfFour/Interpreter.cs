﻿using System;

namespace DesignPatterns
{

    public class Context
    {
        public string Name { get; set; }
        public int Point { get; set; }
    }

    public interface IExpression
    {
        void Interpret(Context context);
    }

    //For L
    public class TerminalExpressionL : IExpression
    {
        public void Interpret(Context context)
        {
            Console.WriteLine("TerminalExpressionL for {0}.", context.Name);
            context.Point += 100;
        }
    }

    //fOR O
    public class TerminalExpressionO : IExpression
    {
        public void Interpret(Context context)
        {
            Console.WriteLine("TerminaExpressionO for {0}.", context.Name);
            context.Point += 1000;
        }
    }
}
