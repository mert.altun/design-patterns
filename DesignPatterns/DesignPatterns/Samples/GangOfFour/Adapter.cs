﻿using System;

namespace DesignPatterns
{
    class Target
    {
        public virtual void Request()
        {
            Console.WriteLine("Called Target Request()");
        }
    }

    //Bu sınıf ile Adaptee nesnesi üzerinden Target ta tanımlı işlemleri
    //gerçekleştirmesini sağlayıp adaptee class ını target a uyarladık
    class Adapter : Target
    {
        private readonly Adaptee _adaptee = new Adaptee();

        public override void Request()
        {
            _adaptee.SpecificRequest();
        }
    }

    class Adaptee
    {
        public void SpecificRequest()
        {
            Console.WriteLine("Called SpecificRequest()");
        }
    }
}
