﻿
namespace DesignPatterns
{
    abstract class Prototype
    {
        // Constructor
        protected Prototype(string id) => Id = id;

        // Gets id
        public string Id { get; }
        public abstract Prototype Clone();
    }

    class ConcretePrototype1 : Prototype
    {
        public ConcretePrototype1(string id) : base(id)
        {
        }

        // Returns a shallow copy
        public override Prototype Clone()
        {
            //.net framework object nesnesi içerisinde bulunan kopyalama metodu
            return (Prototype)this.MemberwiseClone();
        }
    }

    class ConcretePrototype2 : Prototype
    {
        public ConcretePrototype2(string id) : base(id)
        {
        }

        // Returns a shallow copy
        public override Prototype Clone()
        {
            //.net framework object nesnesi içerisinde bulunan kopyalama metodu
            return (Prototype)this.MemberwiseClone();
        }
    }
}
