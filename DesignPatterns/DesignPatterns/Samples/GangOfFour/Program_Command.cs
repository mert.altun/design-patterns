﻿using System;

namespace DesignPatterns
{
    class Program_Command
    {
        static void Main_Command(string[] args)
        {
            //Receiver, command ve invoker nesnelerimizi oluşturalım
            var receiver = new Receiver();
            Command commandAction1 = new ConcreteCommandAction1(receiver);
            Command commandAction2 = new ConcreteCommandAction2(receiver);
            var invoker = new InvokerExecute();

            //Execute mododlarımızı set edelim
            invoker.SetCommand(commandAction1);
            invoker.ExecuteCommand();

            invoker.SetCommand(commandAction2);
            invoker.ExecuteCommand();

            //İhtiyaca göre komutları aynı anda çağıralım
            var invokerExecuteAll=new InvokerExecuteAll();
            invokerExecuteAll.CommandList.Add(commandAction1);
            invokerExecuteAll.CommandList.Add(commandAction2);
            invokerExecuteAll.ExecuteAllCommands();

            Console.ReadKey();
        }
    }
}
