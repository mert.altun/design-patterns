﻿using System;
using System.Collections.Generic;

namespace DesignPatterns
{
    class Program_Interpreter
    {
        static void Main_Interpreter(string[] args)
        {
            //Içerisinde geçen L ve O karakterlerine göre puanlama yapan basit ornek.

            var context = new Context{Name = "L O O D O S"};
            var expressionList = new List<IExpression>();

            foreach (var character in context.Name)
            {
                switch (character)
                {
                    case 'L':
                        expressionList.Add(new TerminalExpressionL());
                        break;
                    case 'O':
                        expressionList.Add(new TerminalExpressionO());
                        break;
                }
            }

            foreach (var item in expressionList)
            {
                item.Interpret(context);
            }

            Console.WriteLine(context.Point);

            Console.ReadKey();
        }
    }
}
