﻿using System;
using System.Collections;

namespace DesignPatterns
{
    public interface IFlyweight
    {
        void StatefulOperation(object o);
    }

    public class FlyweightFactory
    {
        private readonly Hashtable _flyweights = new Hashtable();

        //listede varsa nesneyi döndür yoksa ekle
        public IFlyweight GetFlyweight(string key)
        {
            if (_flyweights.Contains(key))
            {
                return _flyweights[key] as IFlyweight;
            }

            var newFlyweight = new ConcreteFlyweight();

            _flyweights.Add(key, newFlyweight);
            return newFlyweight;
        }
    }

    public class ConcreteFlyweight : IFlyweight
    {
        public void StatefulOperation(object o)
        {
            Console.WriteLine(o);
        }
    }

    public class UnsharedFlyweight : IFlyweight
    {
        private object _state;

        public void StatefulOperation(object o)
        {
            _state = o;
            Console.WriteLine(o);
        }
    }
}
