﻿using System;

namespace DesignPatterns
{
    class Program_Flyweight
    {
        static void Main_Flyweight(string[] args)
        {
            int extrinsicstate = 22;

            FlyweightFactory factory = new FlyweightFactory();

            IFlyweight fx = factory.GetFlyweight("X");
            fx.StatefulOperation(--extrinsicstate);

            IFlyweight fy = factory.GetFlyweight("Y");
            fy.StatefulOperation(--extrinsicstate);

            IFlyweight fz = factory.GetFlyweight("Z");
            fz.StatefulOperation(--extrinsicstate);

            UnsharedFlyweight fu = new UnsharedFlyweight();
            fu.StatefulOperation(--extrinsicstate);

            Console.ReadKey();
        }
    }
}
