﻿using System;
using System.Collections.Generic;

namespace DesignPatterns
{
    abstract class Subject
    {
        private readonly List<Observer> _observers = new List<Observer>();

        public void Attach(Observer observer)
        {
            _observers.Add(observer);
        }

        public void Detach(Observer observer)
        {
            _observers.Remove(observer);
        }

        public void Notify()
        {
            foreach (var o in _observers)
            {
                o.Update();
            }
        }
    }

    class ConcreteSubject : Subject
    {
        public string SubjectState { get; set; }
    }

    abstract class Observer
    {
        public abstract void Update();
    }

    class ConcreteObserver : Observer
    {
        private readonly string _name;
        private string _observerState;
        private readonly ConcreteSubject _subject;

        // Constructor
        public ConcreteObserver(ConcreteSubject subject, string name)
        {
            _subject = subject;
            _name = name;
        }

        public override void Update()
        {
            _observerState = _subject.SubjectState;
            Console.WriteLine("Observer {0}'s new state is {1}",
                _name, _observerState);
        }
    }
}
