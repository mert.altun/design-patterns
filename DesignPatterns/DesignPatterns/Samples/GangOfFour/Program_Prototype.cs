﻿using System;

namespace DesignPatterns
{
    class Program_Prototype
    {
        static void Main_Prototype(string[] args)
        {
            //kopyalanacak nesne
            var p1 = new ConcretePrototype1("I");
            //kopyalanan nesne
            var c1 = (ConcretePrototype1)p1.Clone();
            Console.WriteLine("Cloned: {0}", c1.Id);

            var p2 = new ConcretePrototype2("II");
            var c2 = (ConcretePrototype2)p2.Clone();
            Console.WriteLine("Cloned: {0}", c2.Id);

            Console.ReadKey();
        }
    }
}
