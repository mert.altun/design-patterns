﻿using System;

namespace DesignPatterns
{
    class Program_Builder
    {
        static void Main_Builder(string[] args)
        {
            AssemblyLine shop = new AssemblyLine();

            SandwichBuilder builder = new HamAndCheese();
            shop.Assemble(builder);
            builder.Sandwich.Show();

            builder = new BLT();
            shop.Assemble(builder);
            builder.Sandwich.Show();

            builder = new TurkeyClub();
            shop.Assemble(builder);
            builder.Sandwich.Show();
            Console.ReadKey();
        }
    }
}
