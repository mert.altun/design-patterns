﻿using System;

namespace DesignPatterns
{
    public sealed class TheBell
    {
        private static TheBell _bellConnection;
        //lock objesi
        private static readonly object SyncRoot = new object();
        //Privete constructor (instance sadece Instance metodu ile alınabilsin)
        private TheBell() { }

        public static TheBell Instance
        {
            get
            {
                //Theread safe olması için diğer thread beklenir
                lock (SyncRoot)
                {
                    if (_bellConnection == null)
                    {
                        _bellConnection = new TheBell();
                    }
                }

                return _bellConnection;
            }
        }

        public void Ring()
        {
            Console.WriteLine("Ding! Order up!");
        }
    }
}
