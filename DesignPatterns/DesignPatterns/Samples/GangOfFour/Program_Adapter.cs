﻿using System;

namespace DesignPatterns
{
    class Program_Adapter
    {
        static void Main_Adapter(string[] args)
        {
            Target target = new Adapter();
            target.Request();

            Console.ReadKey();
        }
    }
}
