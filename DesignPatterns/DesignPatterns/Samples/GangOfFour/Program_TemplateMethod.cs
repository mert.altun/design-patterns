﻿using System;

namespace DesignPatterns
{
    class Program_TemplateMethod
    {
        static void Main_TemplateMethod(string[] args)
        {
            AbstractClass aA = new ConcreteClassA();
            aA.TemplateMethod();

            AbstractClass aB = new ConcreteClassB();
            aB.TemplateMethod();

            Console.ReadKey();
        }
    }
}
