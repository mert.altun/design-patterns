﻿using System;

namespace DesignPatterns
{
    class Program_FactoryMethod
    {
        static void Main_FactoryMethod(string[] args)
        {
            var creators = new Creator[2];

            creators[0] = new ConcreteCreatorA();
            creators[1] = new ConcreteCreatorB();

            foreach (var creator in creators)
            {
                var product = creator.FactoryMethod();
                product.Test();
                Console.WriteLine("Created {0}",
                    product.GetType().Name);
            }
            Console.ReadKey();
        }
    }
}
