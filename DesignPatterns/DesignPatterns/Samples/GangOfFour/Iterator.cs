﻿using System;
using System.Collections;

namespace DesignPatterns
{
    //Koleksiyon elemanlarına erişmek için fonksiyonlar tanımlanır.
    public interface ITerator
    {
        object Current { get; }
        bool Next();
    }

    //Koleksi
    public interface IAggregate
    {
        ITerator CreateIterator();
    }

    //Koleksiyon nesnesi ve içerisinde dolaşmayı sağlayan fonksiyonlar yazılır.
    public class ConcreteAggregate : IAggregate
    {
        private readonly ArrayList _items = new ArrayList();

        public ITerator CreateIterator()
        {
            return new ConcreteIterator(this);
        }

        public object this[int index] => _items[index];

        public int Count => _items.Count;

        public void Add(object o)
        {
            _items.Add(o);
        }
    }

    //Iterator arayüzü fonksiyonları uygulanır.
    public class ConcreteIterator : ITerator
    {
        private readonly ConcreteAggregate _aggregate;
        private int _index;

        public ConcreteIterator(ConcreteAggregate aggregate)
        {
            _aggregate = aggregate;
            _index = -1;
        }

        public bool Next()
        {
            _index++;
            return _index < _aggregate.Count;
        }

        public object Current
        {
            get
            {
                if (_index < _aggregate.Count)
                    return _aggregate[_index];

                throw new InvalidOperationException();
            }
        }
    }
}
