﻿using System;

namespace DesignPatterns.State
{
    class Program_State
    {
        static void Main_State(string[] args)
        {
            // Context nesnesi tanımlanır
            var c = new Context(new ConcreteStateA());

            //Her requestte context yenilenir.
            c.Request();
            c.Request();
            c.Request();
            c.Request();

            Console.ReadKey();
        }
    }
}
