﻿using System;

namespace DesignPatterns
{
    class Program_Visitor
    {
        static void Main_Visitor(string[] args)
        {
            // Setup structure
            var o = new ObjectStructure();
            o.Attach(new ConcreteElementA());
            o.Attach(new ConcreteElementB());

            // Create visitor objects
            var v1 = new ConcreteVisitor1();
            var v2 = new ConcreteVisitor2();

            // Structure accepting visitors
            o.Accept(v1);
            o.Accept(v2);

            Console.ReadKey();
        }
    }
}
