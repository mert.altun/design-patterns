﻿using System;

namespace DesignPatterns
{
    class Program_Bridge
    {
        static void Main_Bridge(string[] args)
        {
            Abstraction ab = new RefinedAbstraction();

            // ConcreteImplementorA nesenesini implement edip Operation metodunu çağırıyoruz
            ab.Implementor = new ConcreteImplementorA();
            ab.Operation();

            // ConcreteImplementorB nesenesini implement edip Operation metodunu çağırıyoruz
            ab.Implementor = new ConcreteImplementorB();
            ab.Operation();

            Console.ReadKey();
        }
    }
}
