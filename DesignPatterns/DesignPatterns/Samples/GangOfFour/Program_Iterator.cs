﻿using System;

namespace DesignPatterns
{
    class Program_Iterator
    {
        static void Main_Iterator(string[] args)
        {
            //Aggregate nesnesi oluşturulur ve içirisine koleksiyon elemanları eklenir
            var aggr = new ConcreteAggregate();
            aggr.Add("One");
            aggr.Add("Two");
            aggr.Add("Three");
            aggr.Add("Four");
            aggr.Add("Five");
            
            //iterator nesnesi oluşturularak kolesiyon içerisinde gezilir.
            var iterator = aggr.CreateIterator();

            while (iterator.Next())
            {
                var item = (string)iterator.Current;
                Console.WriteLine(item);
            }

            Console.ReadKey();
        }
    }
}
