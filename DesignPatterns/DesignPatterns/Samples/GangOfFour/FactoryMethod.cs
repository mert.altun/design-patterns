﻿using System;

namespace DesignPatterns
{
    interface IProduct
    {
        void Test();
    }

    class ConcreteProductA : IProduct
    {
        public void Test()
        {
            Console.WriteLine("TestA");
        }
    }

    class ConcreteProductB : IProduct
    {
        public void Test()
        {
            Console.WriteLine("TestB");
        }
    }

    //verilen ConcreteCreator a göre İstediğimiz product nesnesini geri dönücek
    abstract class Creator
    {
        public abstract IProduct FactoryMethod();
    }

    class ConcreteCreatorA : Creator
    {
        public override IProduct FactoryMethod()
        {
            return new ConcreteProductA();
        }
    }

    class ConcreteCreatorB : Creator
    {
        public override IProduct FactoryMethod()
        {
            return new ConcreteProductB();
        }
    }
}
