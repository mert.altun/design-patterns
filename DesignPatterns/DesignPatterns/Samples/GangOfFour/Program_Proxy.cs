﻿using System;

namespace DesignPatterns.Proxy
{
    class Program_Proxy
    {
        static void Main_Proxy(string[] args)
        {

            Subject proxy = new Proxy();
            proxy.PerformAction();

            Console.ReadKey();
        }
    }
}
