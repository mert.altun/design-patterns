﻿using System;

namespace DesignPatterns
{
    class Program_ChainOfResponsibility
    {
        static void Main_ChainOfResponsibility(string[] args)
        {
            //Chain of Responsibility e uygun olarak daha önceden yazmış olduğumuz
            //nesnelerimizi(prensibe göre halkalarımızı) tanımlıyoruz.
            var concreteHandlerA = new ConcreteHandlerA();
            var concreteHandlerB = new ConcreteHandlerB();
            var concreteHandlerC = new ConcreteHandlerC();

            //istek hangi sıra ile iletilsin
            concreteHandlerA.SetSuccessor(concreteHandlerB);
            concreteHandlerB.SetSuccessor(concreteHandlerC);

            //istekler
            concreteHandlerA.HandleRequest(1);
            concreteHandlerA.HandleRequest(11);
            concreteHandlerA.HandleRequest(9);

            Console.ReadKey();
        }
    }
}
