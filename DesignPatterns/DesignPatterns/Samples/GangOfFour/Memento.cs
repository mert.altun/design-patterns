﻿using System;

namespace DesignPatterns
{
    //Kopyasını tutmak istediğimiz özelliklerin bulunduğu memento sınıfı
    class Memento
    {
        // Constructor
        public Memento(string state)
        {
            State = state;
        }

        // Gets or sets state
        public string State { get; }
    }

    //Kopyası tutulacak memento nun referansı tutulut
    class Caretaker
    {
        // Gets or sets memento
        public Memento Memento { set; get; }
    }

    //Memento nesnemizi oluşturup geri yüklemeden sorumlu originator sınıfı
    class Originator
    {
        private string _state;

        // Property
        public string State
        {
            get => _state;
            set
            {
                _state = value;
                Console.WriteLine("State = " + _state);
            }
        }

        // Creates memento 
        public Memento CreateMemento()
        {
            return (new Memento(_state));
        }

        // Restores original state
        public void SetMemento(Memento memento)
        {
            Console.WriteLine("Restoring state...");
            State = memento.State;
        }
    }

}
