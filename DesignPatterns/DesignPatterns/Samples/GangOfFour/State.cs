﻿using System;

namespace DesignPatterns.State
{
    public class Context
    {
        public IState State { get; set; }

        public Context(IState newstate)
        {
            State = newstate;
        }

        public void Request()
        {
            State.Handle(this);
        }
    }


    public interface IState
    {
        void Handle(Context context);
    }

    //Context e ConcreteStateB yi ata
    public class ConcreteStateA : IState
    {
        public void Handle(Context context)
        {
            Console.WriteLine("Handle called from ConcreteStateA");
            context.State = new ConcreteStateB();
        }
    }

    //Context e ConcreteStateA yı ata
    public class ConcreteStateB : IState
    {
        public void Handle(Context context)
        {
            Console.WriteLine("Handle called from ConcreteStateB");
            context.State = new ConcreteStateA();
        }
    }
}
