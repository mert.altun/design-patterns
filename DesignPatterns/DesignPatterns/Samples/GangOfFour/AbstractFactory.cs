﻿
namespace DesignPatterns.AbstractFactory
{
    //Abstract class lar
    abstract class Sandwich { }
    abstract class Dessert { }

    //Abstract nesnelerin yaratma yöneteminin belirlendiği sınıf
    abstract class RecipeFactory
    {
        public abstract Sandwich CreateSandwich();
        public abstract Dessert CreateDessert();
    }

    #region ForAdult
    //Concrete class lar
    class BLT : Sandwich { }
    class CremeBrulee : Dessert { }

    //Abstract nesneler kullanılarak Concrete nesneler yareatılır(Yetişkinler için kullanılacak)
    class AdultCuisineFactory : RecipeFactory
    {
        public override Sandwich CreateSandwich()
        {
            return new BLT();
        }

        public override Dessert CreateDessert()
        {
            return new CremeBrulee();
        }
    }
    #endregion

    #region ForChild
    //Concrete class lar
    class GrilledCheese : Sandwich { }
    class IceCreamSundae : Dessert { }

    //Abstract nesneler kullanılarak Concrete nesneler yareatılır(Çocuklar için kullanılacak)
    class KidCuisineFactory : RecipeFactory
    {
        public override Sandwich CreateSandwich()
        {
            return new GrilledCheese();
        }

        public override Dessert CreateDessert()
        {
            return new IceCreamSundae();
        }
    }
    #endregion



}
