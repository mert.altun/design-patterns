﻿using System;

namespace DesignPatterns.Decorator
{
    class Program_Decorator
    {
        static void Main_Decorator(string[] args)
        {
            //decorator uygulanacak component nesnesi
            ConcreteComponent c = new ConcreteComponent();

            //componentler oluştutulur
            ConcreteDecoratorA d1 = new ConcreteDecoratorA();
            ConcreteDecoratorB d2 = new ConcreteDecoratorB();

            //d1 e component verilir
            d1.SetComponent(c);
            //d2 ye d1 özellikleri verilir
            d2.SetComponent(d1);

            d2.Operation();

            Console.ReadKey();
        }
    }
}
