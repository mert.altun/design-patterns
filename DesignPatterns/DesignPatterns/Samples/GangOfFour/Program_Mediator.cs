﻿using System;

namespace DesignPatterns
{
    class Program_Mediator
    {
        static void Main_Mediator(string[] args)
        {
            //Nesneler arası ilişki için mediator nesnesi oluşturulur.
            var m = new ConcreteMediator();
            //Illşkili nesne1
            var c1 = new ConcreteColleagueA(m);
            //Ilişkili nesne2
            var c2 = new ConcreteColleagueB(m);

            m.Colleague1 = c1;
            m.Colleague2 = c2;

            c1.Send("How are you?");
            c2.Send("Fine, thanks");

            Console.ReadKey();
        }
    }
}
