﻿using System;

namespace DesignPatterns
{
    class Program_Observer
    {
        static void Main_Observer(string[] args)
        {
            // Observer pattern için subject nesnesi oluşturulur
            var s = new ConcreteSubject();
            //Observer listesine nesneler eklenir.
            s.Attach(new ConcreteObserver(s, "X"));
            s.Attach(new ConcreteObserver(s, "Y"));
            s.Attach(new ConcreteObserver(s, "Z"));

            // SubjectState e yeni veri ekle 
            s.SubjectState = "ABC";
            //SubjectState nesnesi değişti diğer neseneleri bilgilendir onlarda değişsin
            s.Notify();

            Console.ReadKey();
        }
    }
}
