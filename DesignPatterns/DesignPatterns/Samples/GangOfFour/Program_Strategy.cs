﻿using System;

namespace DesignPatterns
{
    class Program_Strategy
    {
        static void Main_Strategy(string[] args)
        {
            var client = new Client {Strategy = new ConcreteStrategyA()};
            client.CallAlgorithm();

            var client2 = new Client { Strategy = new ConcreteStrategyB() };
            client2.CallAlgorithm();


            Console.ReadKey();
        }
    }
}
