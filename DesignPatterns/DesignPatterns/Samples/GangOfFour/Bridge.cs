﻿using System;

namespace DesignPatterns
{
    abstract class Abstraction
    {
        // Property
        public Implementor Implementor { get; set; }

        public virtual void Operation()
        {
            Implementor.Operation();
        }
    }

    //Bridge nesnesi
    abstract class Implementor
    {
        public abstract void Operation();
    }

    //Abstraction ı genişletir
    class RefinedAbstraction : Abstraction
    {
        public override void Operation()
        {
            Implementor.Operation();
        }
    }

    //Bridge nesnesini uygulayan sınıf1
    class ConcreteImplementorA : Implementor
    {
        public override void Operation()
        {
            Console.WriteLine("ConcreteImplementorA Operation");
        }
    }

    //Bridge nesnesini uygulayan sınıf2
    class ConcreteImplementorB : Implementor
    {
        public override void Operation()
        {
            Console.WriteLine("ConcreteImplementorB Operation");
        }
    }
}
