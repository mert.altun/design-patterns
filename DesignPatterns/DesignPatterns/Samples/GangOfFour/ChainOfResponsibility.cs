﻿using System;

namespace DesignPatterns
{
    public abstract class Handler
    {
        protected Handler Successor;

        public abstract void HandleRequest(int request);

        public void SetSuccessor(Handler successor)
        {
            Successor = successor;
        }
    }

    public class ConcreteHandlerA : Handler
    {
        public override void HandleRequest(int request)
        {
            //Request 1 gelmişse cevap ver, değilse sonraki hanler a yönlendir
            if (request == 1)
                Console.WriteLine("Handled by ConcreteHandlerA");
            else
            {
                Successor?.HandleRequest(request);
            }
        }
    }

    public class ConcreteHandlerB : Handler
    {
        public override void HandleRequest(int request)
        {
            //Request 10 dan büyük gelmişse cevap ver, değilse sonraki hanler a yönlendir
            if (request > 10)
                Console.WriteLine("Handled by ConcreteHandlerB");
            else
            {
                Successor?.HandleRequest(request);
            }
        }
    }

    public class ConcreteHandlerC : Handler
    {
        public override void HandleRequest(int request)
        {
            //Request 10 dan küçük gelmişse cevap ver, değilse sonraki hanler a yönlendir
            if (request < 10)
                Console.WriteLine("Handled by ConcreteHandlerC");
            else
            {
                Successor?.HandleRequest(request);
            }
        }
    }
}
