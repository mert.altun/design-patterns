﻿using System;
using System.Collections.Generic;

namespace DesignPatterns
{
    //Command pattern e göre komutların bulunacağı sınıf
    class Receiver
    {
        public void Action1()
        {
            Console.WriteLine("Called Receiver.Action1()");
        }

        public void Action2()
        {
            Console.WriteLine("Called Receiver.Action2()");
        }
    }

    //Komutların çalıştırılacağı komut nesenelerinin uygulaması gereken abstract sınıf
    abstract class Command
    {
        protected Receiver Receiver;

        // Constructor
        protected Command(Receiver receiver)
        {
            Receiver = receiver;
        }

        public abstract void Execute();
    }

    //Receiver üzerinde Action1 fonksiyonunu çalıştıracak nesne
    class ConcreteCommandAction1 : Command
    {
        // Constructor
        public ConcreteCommandAction1(Receiver receiver) : base(receiver)
        {
        }

        public override void Execute()
        {
            Receiver.Action1();
        }
    }

    //Receiver üzerinde Action2 fonksiyonunu çalıştıracak nesne
    class ConcreteCommandAction2 : Command
    {
        // Constructor
        public ConcreteCommandAction2(Receiver receiver) : base(receiver)
        {
        }

        public override void Execute()
        {
            Receiver.Action2();
        }
    }

    //Command abctract sınıfını kullanan nesnelerin excute metodunu kullanacak olan nesne.
    class InvokerExecute
    {
        private Command _command;

        public void SetCommand(Command command)
        {
            _command = command;
        }

        public void ExecuteCommand()
        {
            _command.Execute();
        }
    }

    //Command abctract sınıfını kullanan nesneleri list içerisinde tutup  içerisinde dönerek
    //excute metodunu kullanacak olan nesne.
    class InvokerExecuteAll
    {
        public List<Command> CommandList { get; set; }

        public InvokerExecuteAll()
        {
            CommandList = new List<Command>();
        }

        public void ExecuteAllCommands()
        {
            foreach (var command in CommandList)
            {
                command.Execute();
            }
        }
    }
}
