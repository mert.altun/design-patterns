﻿using System;

namespace DesignPatterns
{
    //Nesneler arası kullanılacak fonksiyon yada fonksiyonların bulunacağı arayüz
    public interface IMediator
    {
        void SendMessage(Colleague caller, string msg);
    }

    public abstract class Colleague
    {
        protected IMediator Mediator;

        protected Colleague(IMediator mediator)
        {
            Mediator = mediator;
        }
    }

    //Nesneler arası ilişkiler gerçekleştirilir.
    public class ConcreteMediator : IMediator
    {
        public ConcreteColleagueA Colleague1 { get; set; }

        public ConcreteColleagueB Colleague2 { get; set; }

        public void SendMessage(Colleague caller, string msg)
        {
            if (caller == Colleague1)
                Colleague2.Receive(msg);
            else
                Colleague1.Receive(msg);
        }
    }

    //ConcreteMediator üzerinden ilişkili nesne1
    public class ConcreteColleagueA : Colleague
    {
        public ConcreteColleagueA(IMediator mediator) : base(mediator) { }

        public void Send(string msg)
        {
            Console.WriteLine("A send message:" + msg);
            Mediator.SendMessage(this, msg);
        }

        public void Receive(string msg)
        {
            Console.WriteLine("A receive message:" + msg);
        }
    }

    //ConcreteMediator üzerinden ilişkii nesne2
    public class ConcreteColleagueB : Colleague
    {
        public ConcreteColleagueB(IMediator mediator) : base(mediator) { }

        public void Send(string msg)
        {
            Console.WriteLine("B send message:" + msg);
            Mediator.SendMessage(this, msg);
        }

        public void Receive(string msg)
        {
            Console.WriteLine("B receive message:" + msg);
        }
    }
}
