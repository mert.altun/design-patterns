﻿using System;

namespace DesignPatterns
{
    class Program_Facade
    {
        static void Main_Facade(string[] args)
        {
            var facade = new Facade();

            facade.MethodA();
            facade.MethodB();

            Console.ReadKey();
        }
    }
}
