﻿using System;

namespace DesignPatterns
{
    class Program_Singleton
    {
        static void Main_Singleton(string[] args)
        {
            //Instance static metodu kullanılarak instance alıyoruz
            var bell =TheBell.Instance;
            bell.Ring();

            var bell2 = TheBell.Instance;
            bell2.Ring();

            Console.ReadKey();
        }
    }
}
