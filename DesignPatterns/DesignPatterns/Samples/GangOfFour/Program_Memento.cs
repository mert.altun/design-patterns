﻿using System;

namespace DesignPatterns
{
    class Program_Memento
    {
        private static void Main_Memento(string[] args)
        {
            //origator ile birlikte state nesnesi set edilir
            var o = new Originator {State = "On"};

            // State nesnesinin kopyası alınır
            var c = new Caretaker {Memento = o.CreateMemento()};

            //origator içerisindeki state nesnesi değiştirilir
            o.State = "Off";

            // Kopyaladığımız memento origator e set edilir state eski haline gelir
            o.SetMemento(c.Memento);

            Console.ReadKey();
        }
    }
}
