﻿using System;

namespace DesignPatterns.AbstractFactory
{
    class Program_AbstractFactory
    {
        static void Main_AbstractFactory(string[] args)
        {
            StartPoint:
            Console.WriteLine("Who are you? (A)dult or (C)hild?");
            var input = Console.ReadKey().KeyChar;
            RecipeFactory factory;
            //Hangi factory i kullanalım
            switch (input)
            {
                case 'A':
                    factory = new AdultCuisineFactory();
                    break;

                case 'C':
                    factory = new KidCuisineFactory();
                    break;

                default:
                    throw new NotImplementedException();

            }

            var sandwich = factory.CreateSandwich();
            var dessert = factory.CreateDessert();

            Console.WriteLine("\nSandwich: " + sandwich.GetType().Name);
            Console.WriteLine("Dessert: " + dessert.GetType().Name);

            goto StartPoint;
        }
    }
}
